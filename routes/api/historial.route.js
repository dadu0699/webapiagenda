var express = require('express');
var historial = require('../../model/historial.model');
var services = require('../../services');
var router = express.Router();

router.get('/historial/', services.verificar, function(req, res, next) {
  var idUsuario = req.usuario.idUsuario;
  historial.select(idUsuario, function(historiales) {
    if(typeof historiales !== 'undefined') {
      res.json(historiales);
    } else {
      res.json({"mensaje" : "No hay historial"});
    }
  });
});

module.exports = router;
