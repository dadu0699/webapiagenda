var express = require('express');
var router = express.Router();
var categoria = require('../../model/categoria.model');

router.get('/categoria/', function(req, res, next) {
  categoria.select(function(categorias) {
    if(typeof categorias !== 'undefined') {
      res.json(categorias);
    } else {
      res.json({"mensaje" : "No hay categorias"});
    }
  });
});

router.post('/categoria', function(req, res, next) {
  var data = {
    idCategoria : null,
    nombreCategoria : req.body.nombreCategoria
  }

  categoria.insert(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego la categoria"
      });
    } else {
      res.json({"mensaje":"No se ingreso la categoria"});
    }
  });
});

module.exports = router;
