var database = require("../config/database.config");
var Tarea = {};

Tarea.selectAll = function(callback) {
  if(database) {
		database.query('SELECT * FROM Tarea',
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

Tarea.select = function(idUsuario, callback) {
  if(database) {
		database.query('SELECT * FROM tarea_usuario WHERE idUsuario = ?', idUsuario,
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

Tarea.insert = function(data, callback) {
  if(database) {
    database.query('CALL agregar_tarea(?, ?, ?, ?, ?)',
    [data.idUsuario, data.idEstado, data.titulo, data.descripcion, data.fechaFin],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Tarea.update = function(data, callback){
	if(database) {
		database.query('CALL modificar_tarea(?, ?, ?, ?, ?)',
    [data.idTarea, data.idEstado, data.titulo, data.descripcion, data.fechaFin],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

Tarea.delete = function(idTarea, callback) {
	if(database) {
		database.query('CALL eliminar_tarea(?)', idTarea,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Tarea;
