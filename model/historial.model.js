var database = require("../config/database.config");
var Historial = {};

Historial.select = function(idUsuario, callback) {
  if(database) {
		database.query('SELECT * FROM historial_usuario WHERE idUsuario = ?', idUsuario,
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

module.exports = Historial;
