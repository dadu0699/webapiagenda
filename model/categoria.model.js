var database = require("../config/database.config");
var Categoria = {};

Categoria.select = function(callback) {
  if(database) {
		database.query('SELECT * FROM Categoria', function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

Categoria.insert = function(data, callback) {
  if(database) {
    database.query('INSERT INTO Categoria SET ?', data, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

module.exports = Categoria;
