var database = require("../config/database.config");
var Contacto = {};

Contacto.selectAll = function(callback) {
  if(database) {
		database.query('SELECT * FROM Contacto',
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

Contacto.select = function(idUsuario, callback) {
  if(database) {
		database.query('SELECT * FROM usuario_contacto WHERE idUsuario = ?', idUsuario,
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

Contacto.insert = function(data, callback) {
  if(database) {
    database.query('CALL agregar_usuarioContacto(?, ?, ?, ?, ?, ?, ?)',
    [data.idUsuario, data.nombre, data.apellido, data.direccion, data.telefono, data.correo, data.idCategoria],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Contacto.update = function(data, callback){
	if(database) {
		database.query('CALL modificar_contacto(?, ?, ?, ?, ?, ?, ?)',
    [data.idContacto, data.nombre, data.apellido, data.direccion, data.telefono, data.correo, data.idCategoria],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

Contacto.delete = function(idContacto, callback) {
	if(database) {
		database.query('CALL eliminar_contacto(?)', idContacto,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Contacto;
