CREATE DATABASE Agenda;
USE Agenda;

CREATE TABLE Categoria(
	idCategoria INT NOT NULL AUTO_INCREMENT,
    nombreCategoria VARCHAR(30) NOT NULL,
    PRIMARY KEY (idCategoria)
);

CREATE TABLE Contacto(
	idContacto INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(30) NOT NULL,
    apellido VARCHAR(30) NOT NULL,
    direccion VARCHAR(30) NOT NULL,
    telefono VARCHAR(12) NOT NULL,
    correo VARCHAR(40) NOT NULL,
    idCategoria INT NOT NULL,
    PRIMARY KEY (idContacto),
    FOREIGN KEY (idCategoria) REFERENCES Categoria(idCategoria)ON DELETE CASCADE
);

CREATE TABLE Usuario(
	idUsuario INT NOT NULL AUTO_INCREMENT,
    nick VARCHAR(30) NOT NULL,
    contrasena VARCHAR(30) NOT NULL,
    PRIMARY KEY (idUsuario)
);

CREATE TABLE UsuarioDetalle(
	idUsuarioDetalle INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idContacto INT NOT NULL,
    PRIMARY KEY (idUsuarioDetalle),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)ON DELETE CASCADE,
    FOREIGN KEY (idContacto) REFERENCES Contacto(idContacto)ON DELETE CASCADE
);

CREATE TABLE Historial(
	idHistorial INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    descripcion VARCHAR(255) NOT NULL,
    fecha DATETIME NOT NULL,
    PRIMARY KEY (idHistorial),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)ON DELETE CASCADE
);

CREATE TABLE Estado(
	idEstado INT NOT NULL AUTO_INCREMENT,
    nombreEstado VARCHAR(30) NOT NULL,
    PRIMARY KEY (idEstado)
);

CREATE TABLE Tarea(
	idTarea INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idEstado INT NOT NULL,
    titulo VARCHAR(100) NOT NULL,
    descripcion VARCHAR(255) NOT NULL,
    fechaRegistro DATETIME NOT NULL,
    fechaFin DATETIME NOT NULL,
    PRIMARY KEY (idTarea),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)ON DELETE CASCADE,
    FOREIGN KEY (idEstado) REFERENCES Estado(idEstado)ON DELETE CASCADE
);

-- VISTAS
-- USUARIO CONTACTO
DROP VIEW IF EXISTS usuario_contacto;
CREATE VIEW usuario_contacto AS
	SELECT ud.idUsuario, c.idContacto, c.nombre, c.apellido, c.direccion,
		c.telefono, c.correo, c.idCategoria, cat.nombreCategoria
	FROM UsuarioDetalle ud
		INNER JOIN Contacto c ON ud.idContacto = c.idContacto
    INNER JOIN Categoria cat ON c.idCategoria = cat.idCategoria;

-- TAREA USUARIO
DROP VIEW IF EXISTS tarea_usuario;
CREATE VIEW tarea_usuario AS
	SELECT ut.idTarea, ut.idUsuario, ut.idEstado, e.nombreEstado, ut.titulo,
		ut.descripcion, DATE_FORMAT(ut.fechaRegistro, '%d/%m/%Y %H:%i') AS 'registro',
		DATE_FORMAT(ut.fechaFin, '%m/%d/%Y %H:%i') AS 'fin'
	FROM Tarea ut
		INNER JOIN Estado e ON ut.idEstado = e.idEstado;

-- HISTORIAL USUARIO
DROP VIEW IF EXISTS historial_usuario;
CREATE VIEW historial_usuario AS
	SELECT idHistorial, idUsuario, descripcion,
		DATE_FORMAT(fecha, '%d/%m/%Y %H:%i') AS 'fecha'
	FROM Historial;

-- PROCEDIMIENTOS ALMACENADOS
-- AGREGAR USUARIO
DROP procedure IF EXISTS `agregar_usuario`;
DELIMITER $$
CREATE PROCEDURE `agregar_usuario` (
	IN nombreUsuario VARCHAR(30),
	IN pass VARCHAR(30))
BEGIN
	INSERT INTO usuario(nick, contrasena) VALUES(nombreUsuario, pass);
END$$
DELIMITER ;

-- MODIFICAR USUARIO
DROP procedure IF EXISTS `modificar_usuario`;
DELIMITER $$
CREATE PROCEDURE `modificar_usuario`(
	IN id INT,
	IN nombreUsuario VARCHAR(30),
	IN pass VARCHAR(30))
BEGIN
	UPDATE usuario SET nick = nombreUsuario, contrasena = pass WHERE idUsuario = id;
END$$
DELIMITER ;

-- ELIMINAR USUARIO
DROP procedure IF EXISTS `eliminar_usuario`;
DELIMITER $$
CREATE PROCEDURE `eliminar_usuario`(
	IN id INT)
BEGIN
	DELETE FROM usuario WHERE idUsuario = id;
END$$
DELIMITER ;

-- AGREGAR CATEGORIA
DROP procedure IF EXISTS `agregar_categoria`;
DELIMITER $$
CREATE PROCEDURE `agregar_categoria` (IN nombreC VARCHAR(30))
BEGIN
	INSERT INTO categoria(nombreCategoria) VALUES(nombreC);
END$$
DELIMITER ;

-- AGREGAR CONTACTO
DROP procedure IF EXISTS agregar_usuarioContacto;
DELIMITER $$
CREATE PROCEDURE agregar_usuarioContacto (
	IN idUsuarioN INT,
	IN nombreN VARCHAR(30),
	IN apellidoN VARCHAR(30),
	IN direccionN VARCHAR(30),
	IN telefonoN VARCHAR(12),
	IN correoN VARCHAR(40),
	IN idCategoriaN INT)
BEGIN
	INSERT INTO Contacto(nombre, apellido, direccion, telefono, correo, idCategoria)
		VALUES (nombreN, apellidoN, direccionN, telefonoN, correoN, idCategoriaN);

    SET @idContacto = (SELECT idContacto FROM Contacto WHERE nombre = nombreN AND
		apellido = apellidoN AND correo = correoN ORDER bY idContacto DESC LIMIT 1);

	INSERT INTO UsuarioDetalle(idUsuario, idContacto)
		VALUES (idUsuarioN, @idContacto);
END$$
DELIMITER ;

-- MODIFICAR CONTACTO
DROP procedure IF EXISTS `modificar_contacto`;
DELIMITER $$
CREATE PROCEDURE `modificar_contacto`(
	IN id INT,
	IN nombreN VARCHAR(30),
	IN apellidoN VARCHAR(30),
	IN direccionN VARCHAR(30),
	IN telefonoN VARCHAR(12),
	IN correoN VARCHAR(40),
	IN idCategoriaN INT)
BEGIN
	UPDATE contacto SET nombre=nombreN, apellido=apellidoN, direccion=direccionN,
		telefono=telefonoN, correo=correoN, idCategoria=idCategoriaN
        WHERE idContacto=id;
END$$
DELIMITER ;

-- ELIMINAR CONTACTO
DROP procedure IF EXISTS `eliminar_contacto`;
DELIMITER $$
CREATE PROCEDURE `eliminar_contacto` (
	IN id INT)
BEGIN
	DELETE FROM contacto WHERE idContacto=id;
END$$
DELIMITER ;

-- AGREGAR USUARIODETALLE
DROP procedure IF EXISTS `agregar_usuariodetalle`;
DELIMITER $$
CREATE PROCEDURE `agregar_usuariodetalle` (
	IN idUsuarioN INT,
	IN idContactoN INT)
BEGIN
	INSERT INTO usuariodetalle (idUsuario, idContacto) VALUES (idUsuarioN, idContactoN);
END$$
DELIMITER ;

-- ELIMINAR USUARIODETALLE
DROP procedure IF EXISTS `eliminar_usuariodetalle`;
DELIMITER $$
CREATE PROCEDURE `eliminar_usuariodetalle` (
	IN idUsuarioN INT,
	IN idContactoN INT)
BEGIN
	DELETE FROM usuarioDetalle WHERE idUsuario=idUsuarioN AND idContacto=idContactoN;
END$$
DELIMITER ;

-- AGREGAR TAREA
DROP procedure IF EXISTS `agregar_tarea`;
DELIMITER $$
CREATE PROCEDURE `agregar_tarea` (
	IN idUsuarioN INT,
	IN idEstadoN INT,
	IN tituloN VARCHAR(100),
	IN descripcionN VARCHAR(255),
	IN fechaFinN DATETIME)
BEGIN
	INSERT INTO tarea(idUsuario, idEstado, titulo, descripcion, fechaRegistro, fechaFin)
		VALUES (idUsuarioN, idEstadoN, tituloN, descripcionN, NOW(), fechaFinN);
END$$
DELIMITER ;

-- MODIFICAR TAREA
DROP procedure IF EXISTS `modificar_tarea`;
DELIMITER $$
CREATE PROCEDURE `modificar_tarea` (
	IN idTareaN INT,
	IN idEstadoN INT,
	IN tituloN VARCHAR(100),
	IN descripcionN VARCHAR(255),
	IN fechaFinN DATETIME)
BEGIN
	UPDATE tarea SET idEstado=idEstadoN, titulo=tituloN,
		descripcion=descripcionN, fechaFin=fechaFinN WHERE idTarea=idTareaN;
END$$
DELIMITER ;

-- ELIMINAR TAREA
DROP procedure IF EXISTS `eliminar_tarea`;
DELIMITER $$
CREATE PROCEDURE `eliminar_tarea` (
	IN idTareaN INT)
BEGIN
	DELETE FROM tarea WHERE idTarea=idTareaN;
END$$
DELIMITER ;

-- TRIGGERS
-- MODIFICAR USUARIO AL HISTORIAL
DROP trigger IF EXISTS tg_modificarUsuario;
DELIMITER $$
CREATE TRIGGER tg_modificarUsuario
	BEFORE UPDATE ON usuario
    FOR EACH ROW
BEGIN
    INSERT INTO historial(idUsuario, descripcion, fecha)
		VALUES (OLD.idUsuario, 'Se modifico el usuario y/o contraseña', NOW());
END$$
DELIMITER ;

-- AGREGAR CONTACTO AL HISTORIAL
DROP trigger IF EXISTS tg_agregarContacto;
DELIMITER $$
CREATE TRIGGER tg_agregarContacto
	AFTER INSERT ON usuariodetalle
	FOR EACH ROW
BEGIN
    SET @Datos = (SELECT CONCAT(nombre, ' ', apellido)
		FROM contacto WHERE idContacto = NEW.idContacto);

    INSERT INTO historial(idUsuario, descripcion, fecha)
		VALUES (NEW.idUsuario, CONCAT('Se agrego el contacto: ', @Datos),
			NOW());
END$$
DELIMITER ;

-- ELIMINAR CONTACTO Al HISTORIAL
DROP trigger IF EXISTS tg_eliminarContacto;
DELIMITER $$
CREATE TRIGGER tg_eliminarContacto
	BEFORE DELETE ON contacto
    FOR EACH ROW
BEGIN
	SET @Datos = CONCAT(OLD.nombre, ' ', OLD.apellido);

    SET @idUsuario = (SELECT idUsuario FROM usuariodetalle
		WHERE idContacto = OLD.idContacto);

    INSERT INTO historial(idUsuario, descripcion, fecha)
		VALUES (@idUsuario, CONCAT('Se elimino el contacto: ', @Datos),
			now());
END$$
DELIMITER ;

-- AGREGAR TAREA AL HISTORIAL
DROP trigger IF EXISTS tg_agregarTarea;
DELIMITER $$
CREATE TRIGGER tg_agregarTarea
	AFTER INSERT ON tarea
	FOR EACH ROW
BEGIN
    INSERT INTO historial(idUsuario, descripcion, fecha)
		VALUES (NEW.idUsuario, CONCAT('Se agrego la tarea: ', NEW.titulo),
			NOW());
END$$
DELIMITER ;

-- ELIMINAR TAREA Al HISTORIAL
DROP trigger IF EXISTS tg_eliminarTarea;
DELIMITER $$
CREATE TRIGGER tg_eliminarTarea
	BEFORE DELETE ON Tarea
    FOR EACH ROW
BEGIN
    INSERT INTO historial(idUsuario, descripcion, fecha)
		VALUES (OLD.idUsuario, CONCAT('Se elimino la tarea: ', OLD.titulo),
			NOW());
END$$
DELIMITER ;

-- ACTUALIZAR TAREA Al HISTORIAL
DROP trigger IF EXISTS tg_actualizarTarea;
DELIMITER $$
CREATE TRIGGER tg_actualizarTarea
	AFTER UPDATE ON Tarea
    FOR EACH ROW
BEGIN
	IF OLD.titulo != NEW.titulo THEN
		INSERT INTO historial(idUsuario, descripcion, fecha)
			VALUES (OLD.idUsuario, CONCAT('Se actualizo la tarea: ', OLD.titulo, ' a ', NEW.titulo),
				NOW());
	ELSE
		INSERT INTO historial(idUsuario, descripcion, fecha)
			VALUES (OLD.idUsuario, CONCAT('Se actualizo la tarea: ', OLD.titulo),
				NOW());

	END IF;
END$$
DELIMITER ;

INSERT INTO `agenda`.`estado` (`nombreEstado`) VALUES ('Sin Iniciar');
INSERT INTO `agenda`.`estado` (`nombreEstado`) VALUES ('Iniciado');
INSERT INTO `agenda`.`estado` (`nombreEstado`) VALUES ('Finalizado');
